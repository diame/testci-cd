import { connectionTodataBase } from "../database/database";
import { DATABASE } from "../interfaces/index";

//let db = connectionTodataBase("classicmodels");
// let db: DATABASE;

export const getAllCustomersFromService = async (): Promise<DATABASE> => {
  let db: any = await connectionTodataBase("classicmodels");
  let response = await db.customers.findAll({
    include: [
      {
        model: db.employees,
        as: "salesRepEmployeeNumber_employee",
      },
    ],
  });
  console.log("response", response);
  return response;
};
