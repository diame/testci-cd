import { connectionTodataBase } from "../database/database";
import { DATABASE } from "../interfaces/index";

//let db = connectionTodataBase("classicmodels");
// let db: DATABASE;

export const getAllCustomersFromService = async (): Promise<DATABASE> => {
  let db: any = await connectionTodataBase("classicmodels");
  let response = await db.customers.findAll({
    include: [
      {
        model: db.employees,
        as: "salesRepEmployeeNumber_employee",
      },
    ],
  });

  return response;
};

export const getCostumerFromId = async (id: string): Promise<any> => {
  let db: any = await connectionTodataBase("classicmodels");
  let response = await db.customers.findByPk(id);
  return response;
};

export const filterCustomerBy = async (data: any): Promise<any> => {
  console.log("data", data);
  let db: any = await connectionTodataBase("classicmodels");
  let response = await db.customers.findAll({
    where: {
      country: data.country,
      city: data.city,
    },
  });

  return response;
};
