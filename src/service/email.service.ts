import dotenv from "dotenv";
const config = dotenv.config().parsed;

const nodemailer = require("nodemailer");
const sendgridTransport = require("nodemailer-sendgrid-transport");

export const sendMail = async () => {
  try {
    // Configuration du transporteur en utilisant les informations de connexion SendGrid
    const transport = nodemailer.createTransport(
      sendgridTransport({
        host: config!.APP_EMAIL_HOST,
        port: config!.APP_EMAIL_PORT,
        secure: true,
        auth: {
          api_key: config!.SENDGRID_KEY,
        },
      })
    );

    // Définition des détails de l'email à envoyer
    const mailOptions = {
      from: "diamilesarr2006@gmail.com",
      to: "cheikhou_fofana@live.fr",
      subject: "Subject Line",
      text: "This is a test email sent using nodemailer and sendgrid",
    };

    // Envoi de l'email
    transport.sendMail(mailOptions, (error: any, info: any) => {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info);
      }
    });
  } catch (err: any) {
    console.log("err", err);
    throw new Error(err);
  }
};
