import { assert } from "chai";
import { add, formatePrice, formatekey } from "./add";

describe("Addition", function () {
  it("adds two numbers should return 3", function () {
    assert.equal(add(1, 2), 3);
  });
  it("should equale hello diamile", function () {
    assert.equal(formatekey("diamile"), "hello diamile");
  });
  it("should format price to euro", function () {
    assert.equal(formatePrice(10), "10 €");
  });
});
