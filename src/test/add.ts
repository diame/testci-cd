export const add = (a: number, b: number): number => {
  return a + b;
};

export const formatekey = (name: string): string => {
  return `hello ${name}`;
};

export const formatePrice = (price: number): string => {
  return `${price} €`;
};
