export const _productlines = (sequelize: any, DataTypes: any) => {
  return sequelize.define(
    "productlines",
    {
      productLine: {
        type: DataTypes.STRING(50),
        allowNull: false,
        primaryKey: true,
      },
      textDescription: {
        type: DataTypes.STRING(4000),
        allowNull: true,
      },
      htmlDescription: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      image: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: "productlines",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "productLine" }],
        },
      ],
    }
  );
};
