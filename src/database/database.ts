import { Sequelize } from "sequelize";
import { initModels } from "./models/init-models";
import type { DATABASE } from "../interfaces/index";
let db: DATABASE;

export const connectionTodataBase = async (codeName: string): Promise<any> => {
  try {
    const sequelize = new Sequelize(codeName, "root", "", {
      host: "localhost",
      dialect: "mysql",
    });

    await sequelize.authenticate();

    db = initModels(sequelize);

    return db;
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};
