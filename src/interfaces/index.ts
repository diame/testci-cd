export interface DATABASE {
  customers: any;
  employees: any;
  offices: any;
  orderdetails: any;
  orders: any;
  payments: any;
  productlines: any;
  products: any;
}

export interface FILTER {
  country: string;
  city: string;
}
