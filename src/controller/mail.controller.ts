import { sendMail } from "../service/email.service";
import { NextFunction, Response, Request } from "express";

export const sendMailTo = async (
  _: Request,
  res: Response,
  next: NextFunction
): Promise<any> => {
  try {
    await sendMail();
    res.json("ok");
  } catch (err) {
    console.log("err", err);
    next(err);
  }
};
