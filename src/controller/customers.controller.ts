import { NextFunction, Response, Request } from "express";

//import { FILTER } from "../interfaces/index";
import {
  filterCustomerBy,
  getAllCustomersFromService,
  getCostumerFromId,
} from "../service/customers.service";

export const getAllCustomers = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<any> => {
  try {
    if (req.params.id) {
      let customer = await getCostumerFromId(req.params.id);
      return res.json(customer);
    }
    let customers = await getAllCustomersFromService();
    res.json(customers);
  } catch (err) {
    console.log("err", err);
    next(err);
  }
};

export const getCostumerFromsId = async (
  req: Request,
  res: Response
): Promise<any> => {
  try {
    console.log("params", req.params.id);
    let customer = await getCostumerFromId(req.params.id);
    res.json(customer);
  } catch (err) {
    console.log(err);
  }
};

export const filterCustomerByCountryAndcity = async (
  req: Request,
  res: Response
): Promise<any> => {
  try {
    // console.log("ici");
    console.log("query", req.query);
    let customerFilter = await filterCustomerBy(req.query);
    res.json(customerFilter);
    res.json("ok");
  } catch (err) {
    console.log("err", err);
  }
};
