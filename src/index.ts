import express, { Application } from "express";
import dotenv from "dotenv";
//import colors from "colors";
import { route } from "./routes/route";

const config = dotenv.config().parsed;

const app: Application = express();

console.log("check master");

/**
 * @params route
 * @value "/api"
 */
app.use("/api", route);

app.listen(config!.PORT, () => {
  console.log(" server is running on port 3000");
});
