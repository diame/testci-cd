import express from "express";
import {
  getAllCustomers,
  getCostumerFromsId,
  filterCustomerByCountryAndcity,
} from "../controller/customers.controller";

import { sendMailTo } from "../controller/mail.controller";
let router = express.Router();

router.get("/customer/:id?", getCostumerFromsId);

router.get("/customers/:id?", getAllCustomers);

router.get("/filter", filterCustomerByCountryAndcity);

router.get("/send", sendMailTo);

export const route = router;
