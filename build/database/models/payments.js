"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports._payments = void 0;
const _payments = (sequelize, DataTypes) => {
    return sequelize.define("payments", {
        customerNumber: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: "customers",
                key: "customerNumber",
            },
        },
        checkNumber: {
            type: DataTypes.STRING(50),
            allowNull: false,
            primaryKey: true,
        },
        paymentDate: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        amount: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
        },
    }, {
        sequelize,
        tableName: "payments",
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [{ name: "customerNumber" }, { name: "checkNumber" }],
            },
        ],
    });
};
exports._payments = _payments;
//# sourceMappingURL=payments.js.map