"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initModels = void 0;
const sequelize_1 = require("sequelize");
const customers_1 = require("./customers");
const employees_1 = require("./employees");
const offices_1 = require("./offices");
const orderdetails_1 = require("./orderdetails");
const orders_1 = require("./orders");
const payments_1 = require("./payments");
const productlines_1 = require("./productlines");
const products_1 = require("./products");
const initModels = (sequelize) => {
    var customers = (0, customers_1._customers)(sequelize, sequelize_1.DataTypes);
    var employees = (0, employees_1._employees)(sequelize, sequelize_1.DataTypes);
    var offices = (0, offices_1._offices)(sequelize, sequelize_1.DataTypes);
    var orderdetails = (0, orderdetails_1._orderdetails)(sequelize, sequelize_1.DataTypes);
    var orders = (0, orders_1._orders)(sequelize, sequelize_1.DataTypes);
    var payments = (0, payments_1._payments)(sequelize, sequelize_1.DataTypes);
    var productlines = (0, productlines_1._productlines)(sequelize, sequelize_1.DataTypes);
    var products = (0, products_1._products)(sequelize, sequelize_1.DataTypes);
    orders.belongsToMany(products, {
        as: "productCode_products",
        through: orderdetails,
        foreignKey: "orderNumber",
        otherKey: "productCode",
    });
    products.belongsToMany(orders, {
        as: "orderNumber_orders",
        through: orderdetails,
        foreignKey: "productCode",
        otherKey: "orderNumber",
    });
    orders.belongsTo(customers, {
        as: "customerNumber_customer",
        foreignKey: "customerNumber",
    });
    customers.hasMany(orders, { as: "orders", foreignKey: "customerNumber" });
    payments.belongsTo(customers, {
        as: "customerNumber_customer",
        foreignKey: "customerNumber",
    });
    customers.hasMany(payments, { as: "payments", foreignKey: "customerNumber" });
    customers.belongsTo(employees, {
        as: "salesRepEmployeeNumber_employee",
        foreignKey: "salesRepEmployeeNumber",
    });
    employees.hasMany(customers, {
        as: "customers",
        foreignKey: "salesRepEmployeeNumber",
    });
    employees.belongsTo(employees, {
        as: "reportsTo_employee",
        foreignKey: "reportsTo",
    });
    employees.hasMany(employees, { as: "employees", foreignKey: "reportsTo" });
    employees.belongsTo(offices, {
        as: "officeCode_office",
        foreignKey: "officeCode",
    });
    offices.hasMany(employees, { as: "employees", foreignKey: "officeCode" });
    orderdetails.belongsTo(orders, {
        as: "orderNumber_order",
        foreignKey: "orderNumber",
    });
    orders.hasMany(orderdetails, {
        as: "orderdetails",
        foreignKey: "orderNumber",
    });
    products.belongsTo(productlines, {
        as: "productLine_productline",
        foreignKey: "productLine",
    });
    productlines.hasMany(products, { as: "products", foreignKey: "productLine" });
    orderdetails.belongsTo(products, {
        as: "productCode_product",
        foreignKey: "productCode",
    });
    products.hasMany(orderdetails, {
        as: "orderdetails",
        foreignKey: "productCode",
    });
    return {
        customers,
        employees,
        offices,
        orderdetails,
        orders,
        payments,
        productlines,
        products,
    };
};
exports.initModels = initModels;
//# sourceMappingURL=init-models.js.map