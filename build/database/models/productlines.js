"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports._productlines = void 0;
const _productlines = (sequelize, DataTypes) => {
    return sequelize.define("productlines", {
        productLine: {
            type: DataTypes.STRING(50),
            allowNull: false,
            primaryKey: true,
        },
        textDescription: {
            type: DataTypes.STRING(4000),
            allowNull: true,
        },
        htmlDescription: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        image: {
            type: DataTypes.BLOB,
            allowNull: true,
        },
    }, {
        sequelize,
        tableName: "productlines",
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [{ name: "productLine" }],
            },
        ],
    });
};
exports._productlines = _productlines;
//# sourceMappingURL=productlines.js.map