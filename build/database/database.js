"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectionTodataBase = void 0;
const sequelize_1 = require("sequelize");
const init_models_1 = require("./models/init-models");
let db;
const connectionTodataBase = async (codeName) => {
    try {
        const sequelize = new sequelize_1.Sequelize(codeName, "root", "", {
            host: "localhost",
            dialect: "mysql",
        });
        await sequelize.authenticate();
        db = (0, init_models_1.initModels)(sequelize);
        return db;
    }
    catch (error) {
        console.error("Unable to connect to the database:", error);
    }
};
exports.connectionTodataBase = connectionTodataBase;
//# sourceMappingURL=database.js.map