"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const add_1 = require("./add");
describe("Addition", function () {
    it("adds two numbers should return 3", function () {
        chai_1.assert.equal((0, add_1.add)(1, 2), 3);
    });
    it("should equale hello diamile", function () {
        chai_1.assert.equal((0, add_1.formatekey)("diamile"), "hello diamile");
    });
    it("should format price to euro", function () {
        chai_1.assert.equal((0, add_1.formatePrice)(10), "10 €");
    });
});
//# sourceMappingURL=add.test.js.map