"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.route = void 0;
const express_1 = __importDefault(require("express"));
const customers_controller_1 = require("../controller/customers.controller");
const mail_controller_1 = require("../controller/mail.controller");
let router = express_1.default.Router();
router.get("/customer/:id?", customers_controller_1.getCostumerFromsId);
router.get("/customers/:id?", customers_controller_1.getAllCustomers);
router.get("/filter", customers_controller_1.filterCustomerByCountryAndcity);
router.get("/send", mail_controller_1.sendMailTo);
exports.route = router;
//# sourceMappingURL=route.js.map