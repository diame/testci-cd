"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.filterCustomerByCountryAndcity = exports.getCostumerFromsId = exports.getAllCustomers = void 0;
const customers_service_1 = require("../service/customers.service");
const getAllCustomers = async (req, res, next) => {
    try {
        if (req.params.id) {
            let customer = await (0, customers_service_1.getCostumerFromId)(req.params.id);
            return res.json(customer);
        }
        let customers = await (0, customers_service_1.getAllCustomersFromService)();
        res.json(customers);
    }
    catch (err) {
        console.log("err", err);
        next(err);
    }
};
exports.getAllCustomers = getAllCustomers;
const getCostumerFromsId = async (req, res) => {
    try {
        console.log("params", req.params.id);
        let customer = await (0, customers_service_1.getCostumerFromId)(req.params.id);
        res.json(customer);
    }
    catch (err) {
        console.log(err);
    }
};
exports.getCostumerFromsId = getCostumerFromsId;
const filterCustomerByCountryAndcity = async (req, res) => {
    try {
        console.log("query", req.query);
        let customerFilter = await (0, customers_service_1.filterCustomerBy)(req.query);
        res.json(customerFilter);
        res.json("ok");
    }
    catch (err) {
        console.log("err", err);
    }
};
exports.filterCustomerByCountryAndcity = filterCustomerByCountryAndcity;
//# sourceMappingURL=customers.controller.js.map