"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendMailTo = void 0;
const email_service_1 = require("../service/email.service");
const sendMailTo = async (_, res, next) => {
    try {
        await (0, email_service_1.sendMail)();
        res.json("ok");
    }
    catch (err) {
        console.log("err", err);
        next(err);
    }
};
exports.sendMailTo = sendMailTo;
//# sourceMappingURL=mail.controller.js.map