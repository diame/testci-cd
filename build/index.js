"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const route_1 = require("./routes/route");
const config = dotenv_1.default.config().parsed;
const app = (0, express_1.default)();
app.use("/api", route_1.route);
app.listen(config.PORT, () => {
    console.log(" server is running on port 3000");
});
//# sourceMappingURL=index.js.map