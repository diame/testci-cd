"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllCustomersFromService = void 0;
const database_1 = require("../database/database");
const getAllCustomersFromService = async () => {
    let db = await (0, database_1.connectionTodataBase)("classicmodels");
    let response = await db.customers.findAll({
        include: [
            {
                model: db.employees,
                as: "salesRepEmployeeNumber_employee",
            },
        ],
    });
    console.log("response", response);
    return response;
};
exports.getAllCustomersFromService = getAllCustomersFromService;
//# sourceMappingURL=customers.service%20copy%202.js.map