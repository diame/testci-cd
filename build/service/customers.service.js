"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.filterCustomerBy = exports.getCostumerFromId = exports.getAllCustomersFromService = void 0;
const database_1 = require("../database/database");
const getAllCustomersFromService = async () => {
    let db = await (0, database_1.connectionTodataBase)("classicmodels");
    let response = await db.customers.findAll({
        include: [
            {
                model: db.employees,
                as: "salesRepEmployeeNumber_employee",
            },
        ],
    });
    return response;
};
exports.getAllCustomersFromService = getAllCustomersFromService;
const getCostumerFromId = async (id) => {
    let db = await (0, database_1.connectionTodataBase)("classicmodels");
    let response = await db.customers.findByPk(id);
    return response;
};
exports.getCostumerFromId = getCostumerFromId;
const filterCustomerBy = async (data) => {
    console.log("data", data);
    let db = await (0, database_1.connectionTodataBase)("classicmodels");
    let response = await db.customers.findAll({
        where: {
            country: data.country,
            city: data.city,
        },
    });
    return response;
};
exports.filterCustomerBy = filterCustomerBy;
//# sourceMappingURL=customers.service.js.map