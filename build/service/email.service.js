"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendMail = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const config = dotenv_1.default.config().parsed;
const nodemailer = require("nodemailer");
const sendgridTransport = require("nodemailer-sendgrid-transport");
const sendMail = async () => {
    try {
        const transport = nodemailer.createTransport(sendgridTransport({
            host: config.APP_EMAIL_HOST,
            port: config.APP_EMAIL_PORT,
            secure: true,
            auth: {
                api_key: config.SENDGRID_KEY,
            },
        }));
        const mailOptions = {
            from: "diamilesarr2006@gmail.com",
            to: "cheikhou_fofana@live.fr",
            subject: "Subject Line",
            text: "This is a test email sent using nodemailer and sendgrid",
        };
        transport.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
            }
            else {
                console.log("Email sent: " + info);
            }
        });
    }
    catch (err) {
        console.log("err", err);
        throw new Error(err);
    }
};
exports.sendMail = sendMail;
//# sourceMappingURL=email.service.js.map